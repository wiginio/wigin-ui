/* eslint-disable no-unused-vars */
import axios from 'axios'

let ApiPlugin = {
  install (Vue, options) {
    const _axios = axios.create({
      baseURL: window._config.API_ENDPOINT,
      timeout: 60 * 1000,
      withCredentials: false,
      headers: {
        'X-Auth-Token': localStorage.getItem('_xtoken')
      }
    })

    _axios.interceptors.response.use(response => {
      return response
    }, error => {
      if (error.response.status === 401 || error.response.status === 403) {
        localStorage.removeItem('_xtoken')
        window.location.reload()
      } else {
        return Promise.reject(error)
      }
    })

    Vue.prototype.$api = {
      getOAuthToken (provider) {
        return axios.get(window._config.OAUTH_ENDPOINT + '/' + provider)
      },

      login (username, password) {
        return axios.post(window._config.API_ENDPOINT + '/login', { username, password })
      },

      getClusters () {
        return _axios.get('/clusters')
      },

      getClusterByID (id) {
        return _axios.get(`/clusters/${id}`)
      },

      getClusterNodes (id) {
        return _axios.get(`/clusters/${id}/nodes`)
      },

      getClusterDeploy (cid, did) {
        return _axios.get(`/clusters/${cid}/deploys/${did}`)
      },

      getClusterDeploys (id) {
        return _axios.get(`/clusters/${id}/deploys`)
      },

      getClusterDeployLogs (cid, did) {
        return _axios.get(`/clusters/${cid}/deploys/${did}/logs`)
      },

      getClusterSSHKeyByID (id) {
        return _axios.get(`/clusters/${id}/sshkey`)
      },

      downloadClusterDeployArtifact (cid, did, name) {
        return _axios.get(`/clusters/${cid}/deploys/${did}/artifacts/${name}`)
      },

      generateNewSSHKeyByID (id) {
        return _axios.post(`/clusters/${id}/sshkey/make`)
      },

      createCluster (params) {
        return _axios.post('/clusters', params)
      },

      createClusterNode (id, node) {
        return _axios.post(`/clusters/${id}/nodes`, node)
      },

      createClusterDeploy (id, params) {
        return _axios.post(`/clusters/${id}/deploys`, params)
      },

      updateCluster (id, params) {
        return _axios.put(`/clusters/${id}`, params)
      },

      destroyClusterByID (id) {
        return _axios.delete(`/clusters/${id}`)
      },

      destroyClusterDeploy (cid, did) {
        return _axios.delete(`/clusters/${cid}/deploys/${did}`)
      },

      destroyClusterNode (cid, name) {
        return _axios.delete(`/clusters/${cid}/nodes/${name}`)
      }
    }
  }
}

export default ApiPlugin
