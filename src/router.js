import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'clusters',
      component: () => import(/* webpackChunkName: "clusters" */ './views/Clusters.vue'),
      meta: {
        sidebar: true
      }
    },
    {
      path: '/clusters/:id',
      name: 'clusters.view',
      component: () => import(/* webpackChunkName: "clusters.create" */ './views/ClustersView.vue'),
      meta: {
        sidebar: true
      }
    },
    {
      path: '/sign-in',
      name: 'sign-in',
      component: () => import(/* webpackChunkName: "sign-in" */ './views/SignIn.vue'),
      meta: {
        sidebar: false
      }
    },
    {
      path: '/sign-in/callback',
      name: 'sign-in-callback',
      component: () => import(/* webpackChunkName: "sign-in-callback" */ './views/SignInCallback.vue'),
      meta: {
        sidebar: false
      }
    }
  ]
})
