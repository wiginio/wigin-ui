FROM node:8-alpine as build-stage
WORKDIR /app
COPY package*.json ./
RUN npm install
COPY . .
RUN npm run build

FROM nginx:1.15-alpine as production-stage
COPY --from=build-stage /app/dist /html
COPY ./nginx.conf /etc/nginx/conf.d/default.conf
